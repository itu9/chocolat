create database prodfilmdb;
\c prodfilmdb;
create table film(
    idfilm bigserial primary key,
    nomfilm varchar(100)
);
insert into film(nomfilm) values('Rambo'),('Karate kid'),('Commando'),('Tinatic');

create table plateau(
    idplateau bigserial primary key,
    description varchar(255)
);
insert into plateau(description) values
            ('jungle vietnamienne'),
            ('Champ de mais'),
            ('Maison '),
            ('Dojo '),
            ('Academie militaire '),
            ('Desert Irak'),
            ('Bord'),
            ('Bateau');

create  table film_plateau(
    idfilmPlateau bigserial primary key,
    idfilm integer references film(idfilm),
    idplateau integer references plateau(idplateau)
);

insert into film_plateau values(
    (select idfilm from film where nomfilm='Rambo'),
    (select idplateau from plateau where description='jungle vietnamienne')
);
insert into film_plateau values(
    (select idfilm from film where nomfilm='Commando'),
    (select idplateau from plateau where description='jungle vietnamienne')
);
insert into film_plateau values(
    (select idfilm from film where nomfilm='Karate kid'),
    (select idplateau from plateau where description='Maison')
);
insert into film_plateau values(
    (select idfilm from film where nomfilm='Karate kid'),
    (select idplateau from plateau where description='Dojo')
);
insert into film_plateau values(
    (select idfilm from film where nomfilm='Titanic'),
    (select idplateau from plateau where description='Bord')
);
insert into film_plateau values(
    (select idfilm from film where nomfilm='Titanic'),
    (select idplateau from plateau where description='Bateau')
);
create table scene(
    idscene bigserial primary key,
    nomscene varchar(100),
    description varchar(255),
    idfilmPlateau integer references film_plateau(idfilmPlateau)
);

create table emotion(
    idemotion bigserial primary key,
    description varchar(255)
);
create table personnage(
    idpersonnage bigserial primary key,
    nom varchar(255),
    idfilm integer references film(idfilm),
    sexe boolean 
);
create table action(
    idaction bigserial primary key,
    description varchar(255),
    idemotion integer references emotion(idemotion),
    idscene integer references scene(idscene),
    idpersonnage integer references personnage(idpersonnage)
);
