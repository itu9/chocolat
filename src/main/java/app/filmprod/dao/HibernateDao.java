package app.filmprod.dao;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.*;

import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;

//Hibernate 3.0
public class HibernateDao {

    private SessionFactory sessionFactory;

    public <T> T create(T entity) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.saveOrUpdate(entity);
        transaction.commit();
        session.close();
        return entity;
    }

    public <T> T findById(Class<T> clazz, Serializable id) {
        Session session = sessionFactory.openSession();
        T entity = (T) session.get(clazz, id);
        session.close();
        return entity;
    }

    public <T> List<T> findAll(T obj) {
        Session session = sessionFactory.openSession();
        List<T> results = session.createCriteria(obj.getClass()).list();
        session.close();
        return results;
    }

    public <T> List<T> findWhere(T entity) {
        Session session = sessionFactory.openSession();
        Example example = Example.create(entity).ignoreCase();
        List<T> results = session.createCriteria(entity.getClass()).add(example).list();
        session.close();
        return results;
    }

    public <T> List<T> paginateWhere(T entity, int offset, int size) {
        Session session = sessionFactory.openSession();
        Example example = Example.create(entity).ignoreCase();
        List<T> results = session.createCriteria(entity.getClass())
                .add(example)
                .setFirstResult(offset)
                .setMaxResults(offset + size).list();
        session.close();
        return results;
    }

    public <T> List<T> paginate(Class<T> clazz, int offset, int size) {
        Session session = sessionFactory.openSession();
        List<T> results = session.createCriteria(clazz)
                .setFirstResult(offset)
                .setMaxResults(offset + size).list();
        session.close();
        return results;
    }

    public <T> List<T> paginate(Class<T> clazz, int offset, int size, String orderBy, boolean orderAsc) {
        Session session = sessionFactory.openSession();
        Order order = (orderAsc) ? Order.asc(orderBy) : Order.desc(orderBy);
        List<T> results = session.createCriteria(clazz)
                .addOrder(order)
                .setFirstResult(offset)
                .setMaxResults(offset + size).list();
        session.close();
        return results;
    }

    public <T> List<T> findWhereLikeAndEqual(T obj, HashMap<String, Object> likeParams, HashMap<String, Object> equalParams, Integer offset, Integer size,boolean user) {
        Session session = sessionFactory.openSession();
        Criteria criteria = session.createCriteria(obj.getClass());

        Disjunction likeOr = Restrictions.disjunction();
        if (likeParams != null) {
            for (String key : likeParams.keySet()) {
                likeOr.add(Restrictions.ilike(key, likeParams.get(key)));
            }
            criteria.add(likeOr);
        }

        if (equalParams != null) {
            for (String key : equalParams.keySet()) {
                criteria.add(Restrictions.eq(key, equalParams.get(key)));
            }
        }
        if(user){
            Timestamp currentTimestamp=new java.sql.Timestamp(System.currentTimeMillis());
            criteria.add(Restrictions.le("publication_time",currentTimestamp));
        }

        if (offset != null && size != null) {
            criteria.setFirstResult(offset);
            criteria.setMaxResults(size);
        } else if (offset == null) {
            criteria.setFirstResult(0);
            criteria.setMaxResults(size);
        }
        criteria.addOrder(Order.desc("is_showed"));
        List<T> results = criteria.list();
        session.close();
        return results;
    }

    public int countLikesAndEquals(Object obj, HashMap<String, Object> likeParams, HashMap<String, Object> equalParams,boolean user) {
        Session session = sessionFactory.openSession();
        Criteria criteria = session.createCriteria(obj.getClass());

        Disjunction likeOr = Restrictions.disjunction();
        if (likeParams != null) {
            for (String key : likeParams.keySet()) {
                likeOr.add(Restrictions.ilike(key, likeParams.get(key)));
            }
            criteria.add(likeOr);
        }

        if (equalParams != null) {
            for (String key : equalParams.keySet()) {
                criteria.add(Restrictions.eq(key, equalParams.get(key)));
            }
        }
        if(user){
            Timestamp currentTimestamp= Timestamp.valueOf(LocalDateTime.now());
            criteria.add(Restrictions.le("publication_time",currentTimestamp));
        }

        criteria.setProjection(Projections.rowCount());
        Long count = (Long) criteria.uniqueResult();
        session.close();
        return count.intValue();
    }

    public void deleteById(Class tClass, Serializable id) {
        delete(findById(tClass, id));
    }

    public void delete(Object entity) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.delete(entity);
        transaction.commit();
        session.close();
    }

    public <T> T update(T entity) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        session.saveOrUpdate(entity);
        transaction.commit();
        session.close();
        return entity;
    }


    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
}
