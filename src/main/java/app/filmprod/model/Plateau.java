package app.filmprod.model;

import java.sql.Time;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "Plateau")
public class Plateau extends BaseModel implements Comparable<Plateau>  {
    private String description;

    @Transient
    List<Scene> scenes = new ArrayList<Scene>();

    @Transient
    List<SousPlanning> sousPlanings = new ArrayList<SousPlanning>();

    @Transient
    Time time;

    @Transient
    int jour = 1;

    public Plateau(Integer id) {
        this.setId(id);
    }

    public Plateau() {
    }

    public void reset() {
        this.setScenes(new ArrayList<Scene>());
    }

    public int min(List<ActeurNb> liste) {
        int min = 0;
        for (int i = 1; i < liste.size(); i++) {
            if (liste.get(i).getNb() < liste.get(min).getNb()) {
                min = i;
            }
        }
        return min;
    }

    public int pivot(Personnage personnage) {
        if (this.getScenes() == null || this.getScenes().size() == 0) {
            return -1;
        }
        int min = 0;
        Time time = this.getScenes().get(0).getDuree();
        for (int i = 1; i < this.getScenes().size(); i++) {
            if (this.getScenes().get(i).contains(personnage) && this.getScenes().get(i).getDuree().after(time)) {
                min = i;
                time = this.getScenes().get(i).getDuree();
            }
        }
        return min;
    }

    public void arrange(List<Personnage> personnages, Planning planning, Configuration conf) {
        this.ingore(personnages);
        List<ActeurNb> acteurs = this.evalActeur();
        int min = this.min(acteurs);
        int indPivot = this.pivot(acteurs.get(min));
        Scene scene = this.getScenes().get(indPivot);
        Time time = this.addSousPlaning(personnages, scene, planning, conf.getTime(), conf);
        this.ingore(time);
        this.arrange(personnages, scene, planning, time, conf);
    }

    public void arrange(List<Personnage> personnages, Scene pivot, Planning planing, Time left,
            Configuration conf) {
        this.evaluer(pivot);
        pivot = this.findBest();
        left = this.addSousPlaning(personnages, pivot, planing, left, conf);
        if (this.canContinue()) {
            this.arrange(personnages, pivot, planing, left, conf);
        }
    }

    public boolean canContinue() {
        for (int i = 0; i < this.getScenes().size(); i++) {
            if (!this.getScenes().get(i).isAdded()) {
                return true;
            }
        }
        return false;
    }

    public Time addSousPlaning(List<Personnage> personnages, Scene scene, Planning planning, Time left,Configuration conf) {
        scene.setAdded(50);
        this.addPers(personnages, scene, this.getJour());
        SousPlanning sous = new SousPlanning(scene, planning, this.getJour());
        this.getSousPlanings().add(sous);
        if (scene.getDuree().after(left)) {
            left = conf.getTime();
            this.setJour(this.getJour()+1);
            this.ingore(personnages);
            sous.setJour(this.getJour());
        } else if (this.getSousPlanings().size() > 1) {
            scene.addNode(this.getSousPlanings().get(this.getSousPlanings().size() - 2).getScene());
        }
        sous.setDate(conf.getTime(left));
        Time time = this.minus(left, scene.getDuree());
        return time;
    }

    public void addPers(List<Personnage> pers, Scene scene, int jour) {
        for (int i = 0; i < scene.getActions().size(); i++) {
            if (scene.getActions().get(i).getPersonnage() == null) {
                continue;
            }
            if (!this.inList(scene.getActions().get(i).getPersonnage(), pers, jour)) {
                scene.getActions().get(i).getPersonnage().setJour(jour);
                pers.add(scene.getActions().get(i).getPersonnage());
            }
        }
    }

    public boolean inList(Personnage pers, List<Personnage> liste, int jour) {
        for (int i = 0; i < liste.size(); i++) {
            if (pers.getId()==(liste.get(i).getId()) && liste.get(i).getJour() == jour) {
                return true;
            }
        }
        return false;
    }

    public boolean inList(Personnage pers, List<Personnage> liste) {
        for (int i = 0; i < liste.size(); i++) {
            if (pers.getId()==(liste.get(i).getId())) {
                return true;
            }
        }
        return false;
    }

    public Scene findBest() {
        int min = 0;
        while (min < this.getScenes().size() - 1 && this.getScenes().get(min).isIgnore()) {
            min++;
        }
        for (int i = min + 1; i < this.getScenes().size(); i++) {
            if (this.getScenes().get(i).isIgnore()) {
                continue;
            }
            boolean verif = this.getScenes().get(i).getLibre() < this.getScenes().get(min).getLibre();
            if (verif) {
                min = i;
            } else if (this.getScenes().get(i).getLibre() == this.getScenes().get(min).getLibre()) {
                if (this.getScenes().get(i).getDuree().after(this.getScenes().get(min).getDuree())) {
                    min = i;
                }
            }
        }
        return this.getScenes().get(min);
    }

    public void evaluer(Scene pivot) {
        for (int i = 0; i < this.getScenes().size(); i++) {
            if (this.getScenes().get(i).isIgnore()) {
                continue;
            }
            this.getScenes().get(i).couple(pivot);
        }
    }

    public void unpassed() {
        for (int i = 0; i < this.getScenes().size(); i++) {
            this.getScenes().get(i).setIngore(0);
        }
    }

    public void ingore(Time left) {
        for (int i = 0; i < this.getScenes().size(); i++) {
            this.getScenes().get(i).ingore(left);
        }
    }

    public List<ActeurNb> evalActeur() {
        List<ActeurNb> result = new ArrayList<ActeurNb>();
        for (int i = 0; i < this.getScenes().size(); i++) {
            this.getScenes().get(i).calcul(result);
        }
        return result;
    }

    public void ingore(List<Personnage> personnages) {
        for (int i = 0; i < this.getScenes().size(); i++) {
            this.getScenes().get(i).ingore(personnages, this.getJour());
        }
    }

    public void evalTime() {
        Time time = Time.valueOf("00:00:00");
        for (int i = 0; i < this.getScenes().size(); i++) {
            time = this.add(time, this.getScenes().get(i).getDuree());
        }
        this.setTime(time);
    }

    public Time minus(Time time1, Time time2) {
        LocalTime t1 = time1.toLocalTime();
        LocalTime t2 = time2.toLocalTime();
        t1 = t1.minusSeconds(t2.getSecond());
        t1 = t1.minusMinutes(t2.getMinute());
        t1 = t1.minusHours(t2.getHour());
        return Time.valueOf(t1);
    }

    public Time add(Time time1, Time time2) {
        LocalTime t1 = time1.toLocalTime();
        LocalTime t2 = time2.toLocalTime();
        t1 = t1.plusSeconds(t2.getSecond());
        t1 = t1.plusMinutes(t2.getMinute());
        t1 = t1.plusHours(t2.getHour());
        return Time.valueOf(t1);
    }

    public static void main(String[] args) {
        Plateau plateau = new Plateau();
        Time time1 = Time.valueOf("02:00:00");
        Time time2 = Time.valueOf("03:15:00");
        System.out.println(plateau.minus(time2, time1));
    }

    public void addScene(Scene scene) throws NotBelongs {
        if (scene.getPlateau().getId()==(this.getId())) {
            this.getScenes().add(scene);
            return;
        }
        throw new NotBelongs();
    }

    @Override
    public int compareTo(Plateau arg0) {
        return this.getTime().compareTo(arg0.getTime());
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    
    public List<Scene> getScenes() {
        return scenes;
    }

    public void setScenes(List<Scene> scenes) {
        this.scenes = scenes;
    }

    public List<SousPlanning> getSousPlanings() {
        return sousPlanings;
    }

    public void setSousPlanings(List<SousPlanning> sousPlanings) {
        this.sousPlanings = sousPlanings;
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public int getJour() {
        return jour;
    }

    public void setJour(int jour) {
        this.jour = jour;
    }

}
