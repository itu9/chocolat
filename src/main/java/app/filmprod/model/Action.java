package app.filmprod.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "action")
public class Action extends BaseModel {
    // private int idemotion;
    // private int idscene;
    // private int idpersonnage;
    private String description;
    @OneToOne
    @JoinColumn(name = "idemotion", nullable = false)
    Emotion emotion;

    @OneToOne
    @JoinColumn(name = "idpersonnage", nullable = false)
    Personnage personnage;

    @ManyToOne
    @JoinColumn(name = "idscene", nullable = false)
    Scene scene;
    
    public Action(String description, Emotion emotion, Personnage personnage, Scene scene) {
        this.setDescription(description);
        this.setEmotion(emotion);
        this.setPersonnage(personnage);
        this.setScene(scene);
    }

    // public int getIdemotion() {
    // return idemotion;
    // }

    // public void setIdemotion(int idemotion) {
    // this.idemotion = idemotion;
    // }

    // public int getIdscene() {
    // return idscene;
    // }

    // public void setIdscene(int idscene) {
    // this.idscene = idscene;
    // }

    // public int getIdpersonnage() {
    // return idpersonnage;
    // }

    // public void setIdpersonnage(int idpersonnage) {
    // this.idpersonnage = idpersonnage;
    // }

    public Emotion getEmotion() {
        return emotion;
    }

    public void setEmotion(Emotion emotion) {
        this.emotion = emotion;
    }

    public Personnage getPersonnage() {
        return personnage;
    }

    public void setPersonnage(Personnage personnage) {
        this.personnage = personnage;
    }

    public Scene getScene() {
        return scene;
    }

    public void setScene(Scene scene) {
        this.scene = scene;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
