package app.filmprod.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import java.sql.Time;
import java.util.List;

@Entity
@Table(name = "Scene")
public class Scene extends BaseModel {
    // private int idplateau;
    // private int idfilm;
    private String nomscene;
    private String description;
    private Time duree;
    @OneToOne
    @JoinColumn(name = "idfilm", nullable = false)
    Film film;
    @ManyToOne(optional = false)
    @JoinColumn(name = "idplateau", nullable = false)
    Plateau plateau;

    @OneToMany(cascade = CascadeType.PERSIST, mappedBy = "scene")
    List<Action> actions;

    @Transient
    Integer libre = 0;

    @Transient
    Integer ingore = 0; // occupe le scene

    @Transient
    Integer passed = 0; // mihotra le temps

    @Transient
    Integer added = 0;

    public void couple(Scene scene) {
        int nb = 0;
        int matched = 0;
        for (int i = 0; i < this.getActions().size(); i++) {
            if (this.getActions().get(i).getPersonnage() == null) {
                continue;
            }
            nb++;
            if (scene.contains(this.getActions().get(i).getPersonnage())) {
                matched++;
            }
        }
        this.setLibre(nb - matched);
    }

    public boolean contains(Personnage pers) {
        for (int i = 0; i < this.getActions().size(); i++) {
            if (this.getActions().get(i).getPersonnage() == null) {
                continue;
            }
            if (this.getActions().get(i).getPersonnage().getId() == pers.getId()) {
                return true;
            }
        }
        return false;
    }

    public Scene(Integer id,String nomscene, Time duree, Plateau plateau) {
        this.setNomscene(nomscene);
        this.setDuree(duree);
        this.setPlateau(plateau);
        this.setId(id);
    }

    public boolean isIgnore() {
        return this.isBusy() || this.isAdded() || this.isPassed();
    }

    public boolean isPassed() {
        return this.getPassed().compareTo(0) > 0;
    }

    public boolean isAdded() {
        return this.getAdded().compareTo(0) > 0;
    }

    public boolean isBusy() {
        return this.getIngore().compareTo(0) > 0;
    }

    public void ingore(List<Personnage> personnages, int jour) {
        this.setIngore(0);
        for (int i = 0; i < personnages.size(); i++) {
            if (personnages.get(i).getJour() != jour)
                continue;
            this.ignore(personnages.get(i));
        }
    }

    public void addNode(Scene scene) {
        for (int i = 0; i < scene.getActions().size(); i++) {
            if (scene.getActions().get(i).getPersonnage() == null) {
                this.getActions().add(scene.getActions().get(i));
                continue;
            }
            if (!this.contains(scene.getActions().get(i).getPersonnage())) {
                this.getActions().add(scene.getActions().get(i));
            }
        }
    }

    public void ignore(Personnage pers) {
        for (int i = 0; i < this.getActions().size(); i++) {
            if (this.getActions().get(i).getPersonnage() == null) {
                continue;
            }
            if (this.getActions().get(i).getPersonnage().getId() == pers.getId()) {
                this.setIngore(50);
                return;
            }
        }
    }

    public void ingore(Time left) {
        this.setPassed(0);
        if (this.getDuree().after(left)) {
            this.setPassed(50);
        }
    }

    public ActeurNb getActeur(List<ActeurNb> liste, Action action) {
        for (int i = 0; i < liste.size(); i++) {
            if (action.getPersonnage() == null) {
                break;
            }
            if (liste.get(i).getId() == action.getPersonnage().getId()) {
                return liste.get(i);
            }
        }
        ActeurNb acteur = new ActeurNb();
        acteur.setId(action.getPersonnage().getId());
        liste.add(acteur);
        return acteur;
    }

    public void calcul(List<ActeurNb> actNb) {
        for (int i = 0; i < this.getActions().size(); i++) {
            Action action = this.getActions().get(i);
            if (action.getPersonnage() == null)
                continue;
            ActeurNb act = this.getActeur(actNb, action);
            act.setNb(act.getNb() + this.getActions().size() - 1);
        }
    }

    @Override
    public String toString() {
        return "Scene [nomscene=" + nomscene + ", description=" + description + ", duree=" + duree + ", film=" + film
                + ", actions=" + actions + ", ingore=" + ingore + ", added=" + added + "]";
    }
    // public int getIdplateau() {
    // return idplateau;
    // }

    // public void setIdplateau(int idplateau) {
    // this.idplateau = idplateau;
    // }

    // public int getIdfilm() {
    // return idfilm;
    // }

    // public void setIdfilm(int idfilm) {
    // this.idfilm = idfilm;
    // }

    public String getNomscene() {
        return nomscene;
    }

    public void setNomscene(String nomscene) {
        this.nomscene = nomscene;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Time getDuree() {
        return duree;
    }

    public void setDuree(Time duree) {
        this.duree = duree;
    }

    public Film getFilm() {
        return film;
    }

    public void setFilm(Film film) {
        this.film = film;
    }

    public Plateau getPlateau() {
        return plateau;
    }

    public void setPlateau(Plateau plateau) {
        this.plateau = plateau;
    }

    public List<Action> getActions() {
        return actions;
    }

    public void setActions(List<Action> actions) {
        this.actions = actions;
    }

    public Integer getLibre() {
        return libre;
    }

    public void setLibre(Integer libre) {
        this.libre = libre;
    }

    public Integer getIngore() {
        return ingore;
    }

    public void setIngore(Integer ingore) {
        this.ingore = ingore;
    }

    public Integer getPassed() {
        return passed;
    }

    public void setPassed(Integer passed) {
        this.passed = passed;
    }

    public Integer getAdded() {
        return added;
    }

    public void setAdded(Integer added) {
        this.added = added;
    }
    
}
