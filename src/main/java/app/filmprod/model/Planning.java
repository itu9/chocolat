package app.filmprod.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "Planning")
public class Planning extends BaseModel {
    private int idfilm;
    private Timestamp datecreation;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "planning")
    private List<SousPlanning> sousPlanings;

    public Planning() {
        this.setDatecreation(Timestamp.valueOf(LocalDateTime.now()));
    }

    public int getIdfilm() {
        return idfilm;
    }

    public void setIdfilm(int idfilm) {
        this.idfilm = idfilm;
    }

    public Timestamp getDatecreation() {
        return datecreation;
    }

    public void setDatecreation(Timestamp datecreation) {
        this.datecreation = datecreation;
    }

    public List<SousPlanning> getSousPlanings() {
        return sousPlanings;
    }

    public void setSousPlanings(List<SousPlanning> sousPlanings) {
        this.sousPlanings = sousPlanings;
    }

    @Override
    public String toString() {
        return "Planning [idfilm=" + idfilm + ", datecreation=" + datecreation + ", sousPlanings=" + sousPlanings + "]";
    }
    
}
