package app.filmprod.model;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "Film")
public class Film extends BaseModel {
    private String nomfilm;

    @Transient
    List<Scene> scenes;

    public Planning generatePlanning() throws NotBelongs {
        Configuration conf = new Configuration();
        conf.setTime(Time.valueOf("08:00:00"));
        conf.setDebut(Time.valueOf("08:00:00"));
        Planning planning = new Planning();
        planning.setIdfilm(this.getId());
        // avahana par plateau
        List<Plateau> plateaus = this.explodeToPLateau();
        // alahatra par ordre de duree
        this.arrange(plateaus);
        List<Personnage> personnages = new ArrayList<Personnage>();
        for (int i =  plateaus.size()-1; i >= 0; i--) {
            plateaus.get(i).arrange(personnages, planning, conf);
        }
        List<SousPlanning> sousPlanings = this.extract(plateaus);
        planning.setSousPlanings(sousPlanings);
        return planning;
    }

    public List<SousPlanning> extract(List<Plateau> plateaus) {
        List<SousPlanning> answer = new ArrayList<SousPlanning>();
        for (Plateau plateau : plateaus) {
            answer.addAll(plateau.getSousPlanings());
        }
        return answer;
    }

    public void arrange(List<Plateau> plateaus) {
        for (int i = 0; i < plateaus.size(); i++) {
            plateaus.get(i).evalTime();
        }
        Collections.sort(plateaus);
    }

    public List<Plateau> explodeToPLateau() throws NotBelongs {
        List<Plateau> answer = new ArrayList<>();
        int indice = 0;
        for (Scene scene : this.getScenes()) {
            for (int i = 0; i < answer.size(); i++) {
                try {
                    answer.get(i).addScene(scene);
                    indice = 1;
                    break;
                } catch (NotBelongs e) {
                }
            }
            if (indice == 0) {
                Plateau plateau = scene.getPlateau();
                plateau.reset();
                plateau.addScene(scene);
                answer.add(plateau);
            }
            indice = 0;
        }
        return answer;
    }

    public String getNomfilm() {
        return nomfilm;
    }

    public void setNomfilm(String nomfilm) {
        this.nomfilm = nomfilm;
    }

    public List<Scene> getScenes() {
        return scenes;
    }

    public void setScenes(List<Scene> scenes) {
        this.scenes = scenes;
    }
    public static void main(String[] args) throws NotBelongs {
        Film film = new Film();
        film.setId(5);
        film.setNomfilm("The mentalist");
        film.setScenes(Film.generate());
        Planning planning = film.generatePlanning();
        System.out.println(planning);
    }
    public static List<Scene> generate() {
        List<Personnage> personnages = new ArrayList<Personnage>();
        personnages.add(new Personnage(1, "Walter O'Brian", true));
        personnages.add(new Personnage(2, "Tobias Curtis", true));
        personnages.add(new Personnage(3, "Cabe Galo", true));
        personnages.add(new Personnage(4, "Silvester Dod", true));
        personnages.add(new Personnage(5, "Ralf Deening", true));
        personnages.add(new Personnage(6, "Page Deening", false));
        personnages.add(new Personnage(7, "Happy Queen", false));
        personnages.add(new Personnage(8, "Florence", false));

        List<Scene> scenes = new ArrayList<Scene>();
        Plateau plateau1 = new Plateau(1);
        Plateau plateau2 = new Plateau(2);
        Plateau plateau3 = new Plateau(3);
        List<Action> actions = null;
        /*---------------------------Scene 1----------------------------- */
        Scene scene = new Scene(1,"Dejeuner de famille", Time.valueOf("3:00:00"), plateau1);
        actions = new ArrayList<Action>();
        actions.add(new Action("mange du miel", new Emotion("Content"), personnages.get(0), scene));
        actions.add(new Action("parle tout seule", new Emotion("Se sent seul"), personnages.get(1), scene));
        scene.setActions(actions);
        scenes.add(scene);

        /*---------------------------Scene 2----------------------------- */
        scene = new Scene(2,"Dejeuner de famille1", Time.valueOf("4:00:00"), plateau1);
        actions = new ArrayList<Action>();
        actions.add(new Action("prend le café", new Emotion("Calme"), personnages.get(0), scene));
        actions.add(new Action("parle tout seule", new Emotion("Tezitra be"), personnages.get(3), scene));
        scene.setActions(actions);
        scenes.add(scene);

        scene = new Scene(14,"Dejeuner de famille2", Time.valueOf("7:00:00"), plateau1);
        actions = new ArrayList<Action>();
        actions.add(new Action("prend le café", new Emotion("Calme"), personnages.get(2), scene));
        actions.add(new Action("parle tout seule", new Emotion("Tezitra be"), personnages.get(6), scene));
        scene.setActions(actions);
        scenes.add(scene);

        /*---------------------------Scene 3----------------------------- */
        scene = new Scene(3,"Kaly", Time.valueOf("4:00:00"), plateau2);
        actions = new ArrayList<Action>();
        actions.add(new Action("Fait du math au lieu de mangé", new Emotion("Tezitra be"), personnages.get(3), scene));
        actions.add(new Action("Dit à Ralf:\"Mange au lieu de jouer au telescope.\"", new Emotion("Inquiette"),
                personnages.get(7), scene));
        scene.setActions(actions);
        scenes.add(scene);

        /*---------------------------Scene 4----------------------------- */
        scene = new Scene(4,"Heure de repos", Time.valueOf("2:00:00"), plateau2);
        actions = new ArrayList<Action>();
        actions.add(new Action("tape son moto", new Emotion("Tezitra be"), personnages.get(1), scene));
        actions.add(new Action("Fait une experience sur les panneaux solaires", new Emotion("Concentré"),
                personnages.get(3), scene));
        scene.setActions(actions);
        scenes.add(scene);

        /*---------------------------Scene 5----------------------------- */
        scene = new Scene(5,"Enquette", Time.valueOf("6:00:00"), plateau3);
        actions = new ArrayList<Action>();
        actions.add(new Action("appel l'equipe", new Emotion("Sérieux"), personnages.get(1), scene));
        actions.add(new Action("Une voiture passe", new Emotion("Tezitra be"), null, scene));
        scene.setActions(actions);
        scenes.add(scene);

        /*---------------------------Scene 6----------------------------- */
        scene = new Scene(6,"Batiment de la CIA", Time.valueOf("4:00:00"), plateau3);
        actions = new ArrayList<Action>();
        actions.add(new Action("une bombe est decouvert et on a plus que 5 minutes", new Emotion("Tezitra be"), null,
                scene));
        actions.add(new Action("Désamorce la bombe", new Emotion("Tezitra be"), personnages.get(5), scene));
        scene.setActions(actions);
        scenes.add(scene);
        return scenes;
    }

}
