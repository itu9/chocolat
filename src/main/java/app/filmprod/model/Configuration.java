package app.filmprod.model;

import java.sql.Time;
import java.time.LocalTime;

public class Configuration {
    Time time;
    Time debut;

    public Time getTime(Time left) {
        Time t = this.minus(this.getTime(), left);
        return this.add(this.getDebut(), t);
    }

    public Time minus(Time time1, Time time2) {
        LocalTime t1 = time1.toLocalTime();
        LocalTime t2 = time2.toLocalTime();
        t1 = t1.minusSeconds(t2.getSecond());
        t1 = t1.minusMinutes(t2.getMinute());
        t1 = t1.minusHours(t2.getHour());
        return Time.valueOf(t1);
    }

    public Time add(Time time1, Time time2) {
        LocalTime t1 = time1.toLocalTime();
        LocalTime t2 = time2.toLocalTime();
        t1 = t1.plusSeconds(t2.getSecond());
        t1 = t1.plusMinutes(t2.getMinute());
        t1 = t1.plusHours(t2.getHour());
        return Time.valueOf(t1);
    }

    public Time getTime() {
        return time;
    }

    public void setTime(Time time) {
        this.time = time;
    }

    public Time getDebut() {
        return debut;
    }

    public void setDebut(Time debut) {
        this.debut = debut;
    }

}
