package app.filmprod.model;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "Emotion")
public class Emotion extends BaseModel{
    private String description;

    public Emotion(String description) {
        this.description = description;
    }

    public Emotion() {
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
