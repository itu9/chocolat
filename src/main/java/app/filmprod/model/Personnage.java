package app.filmprod.model;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "personnage")
public class Personnage extends BaseModel {
    private int idfilm;
    private String nom;
    private boolean sexe;

    private int idperson;

    @Transient
    int jour;

    public Personnage() {
    }

    public Personnage( int id,String nom, boolean sexe) {
        this.setNom(nom);
        this.setSexe(sexe);
        this.setId(id);
    }

    public int getIdfilm() {
        return idfilm;
    }

    public void setIdfilm(int idfilm) {
        this.idfilm = idfilm;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public boolean isSexe() {
        return sexe;
    }

    public void setSexe(boolean sexe) {
        this.sexe = sexe;
    }

    public int getIdperson() {
        return idperson;
    }

    public void setIdperson(int idperson) {
        this.idperson = idperson;
    }

    public int getJour() {
        return jour;
    }

    public void setJour(int jour) {
        this.jour = jour;
    }
    
}
