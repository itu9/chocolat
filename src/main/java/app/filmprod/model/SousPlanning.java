package app.filmprod.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@Entity
@Table(name = "SousPlanning")
public class SousPlanning extends BaseModel {
    @OneToOne
    @JoinColumn(name = "idscene", nullable = false)
    Scene scene;
    private Timestamp datecreation;
    private int idplanning;

    Integer jour;

    Time date;

    @Override
    public String toString() {
        return "\n\nSousPlanning [\nplateau : "+scene.getPlateau().getId()+",idscene: "+scene.getId()+",scene=" + scene + ", datecreation=" + datecreation + ", idplanning=" + idplanning
                + ", jour=" + jour + ", date = "+this.date+"\n]";
    }

    public SousPlanning(Scene scene, Planning planning, int jour) {
        this();
        this.setScene(scene);
        this.setIdplanning(planning.getId());
        this.setJour(jour);
    }

    public SousPlanning() {
        this.setDatecreation(Timestamp.valueOf(LocalDateTime.now()));
    }

    public Timestamp getDatecreation() {
        return datecreation;
    }

    public void setDatecreation(Timestamp datecreation) {
        this.datecreation = datecreation;
    }

    public int getIdplanning() {
        return idplanning;
    }

    public void setIdplanning(int idplanning) {
        this.idplanning = idplanning;
    }

    public Scene getScene() {
        return scene;
    }

    public void setScene(Scene scene) {
        this.scene = scene;
    }

    public Integer getJour() {
        return jour;
    }

    public void setJour(Integer jour) {
        this.jour = jour;
    }

    public Time getDate() {
        return date;
    }

    public void setDate(Time date) {
        this.date = date;
    }

}
