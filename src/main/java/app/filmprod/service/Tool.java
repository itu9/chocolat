package app.filmprod.service;

import org.apache.commons.fileupload.FileItem;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;

import javax.servlet.http.HttpSession;
import java.io.File;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.List;

public class Tool {
    private static final String UPLOAD_DIRECTORY = "C:\\Users\\ranja\\OneDrive\\Documents\\GitHub\\contentmanaging\\src\\main\\webapp\\serverimages";

    public static String buildErrorMessage(BindingResult bindingResult) {
        StringBuilder message = new StringBuilder();
        bindingResult.getAllErrors().forEach(error -> {
            message.append(error.getDefaultMessage());
            message.append(" : ");
            message.append(error.getObjectName());
            message.append("\n");
        });
        return message.toString();
    }

    public static String hashPassword(String password) throws NoSuchAlgorithmException {
        return BCrypt.toHexString(BCrypt.getSHA(password));
    }

    public static String getHtmlErrorMessage(String message) {
        return "<div class=\"alert alert-danger\" role=\"alert\">\n" +
                "  <h4 class=\"alert-heading\">Error!</h4>\n" +
                "  <p>" + message + "</p>\n" +
                "</div>";
    }

    public static String getHtmlSuccessMessage(String message) {
        return "<div class=\"alert alert-success\" role=\"alert\">\n" +
                "  <h4 class=\"alert-heading\">Success!</h4>\n" +
                "  <p>" + message + "</p>\n" +
                "</div>";
    }

    public static void setMessage(Model model, BindingResult bindingResult, boolean error) {
        String message = Tool.buildErrorMessage(bindingResult);
        if (!error) {
            message = Tool.getHtmlSuccessMessage(message);
        } else {
            message = Tool.getHtmlErrorMessage(message);
        }
        model.addAttribute("message", message);
    }

    public static String returnBack(String login, Model model, BindingResult bindingResult, boolean error) {
        Tool.setMessage(model, bindingResult, error);
        return login;
    }

    public static void setMessage(Exception e, Model model, boolean error) {
        String message = e.getMessage();
        if (!error) {
            message = Tool.getHtmlSuccessMessage(message);
        } else {
            message = Tool.getHtmlErrorMessage(message);
        }
        model.addAttribute("message", message);
    }

    public static String getBase64String(byte[] fileContent) {
        return Base64.getEncoder().encodeToString(fileContent);
    }

    public static void setMessage(Exception e, HttpSession session, boolean error) {
        String message = e.getMessage();
        if (!error) {
            message = Tool.getHtmlSuccessMessage(message);
        } else {
            message = Tool.getHtmlErrorMessage(message);
        }
        session.setAttribute("message", message);
    }

    public static String writeFile(List<FileItem> multiparts) throws Exception {
        String path = "";
        String name = "";
        for (FileItem item : multiparts) {
            if (!item.isFormField()) {
                File file = new File(item.getName());
                // check file size less than 10MB
                if (file.length() > 10000000) {
                    throw new Exception("File size must be less than 10MB");
                } else if (!file.getName().endsWith(".jpg") && !file.getName().endsWith(".png") && !file.getName().endsWith(".jpeg")) {
                    throw new Exception("File must be an image");
                }
                name = file.getName();
                path = UPLOAD_DIRECTORY + File.separator + name;
                item.write(new File(path));
            }
        }
        return name;
    }

    public static String getCurrentTimestamp(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
        return dateFormat.format(new Date());
    }

}
