<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<c:import url="Header.jsp"/>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-12 col-md-5 col-xl-4 my-5">

            <!-- Heading -->
            <h1 class="display-4 text-center mb-3">
                Sign in
            </h1>

            <!-- Subheading -->
            <p class="text-muted text-center mb-5">
                Free access to our admin dashboard.
            </p>

        </div>
    </div> <!-- / .row -->
</div>
<c:import url="Footer.jsp"/>