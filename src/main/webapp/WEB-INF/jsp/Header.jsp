<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc."/>

    <!-- Favicon -->
    <link rel="shortcut icon" href="<c:url value="/resources/assets/favicon/favicon.ico" />" type="image/x-icon"/>

    <!-- Map CSS -->
    <link rel="stylesheet" href="<c:url value="/resources/assets/css/mapbox-gl.css" />"/>

    <!-- Libs CSS -->
    <link rel="stylesheet" href="<c:url value="/resources/assets/css/libs.bundle.css" />"/>

    <!-- Theme CSS -->
    <link rel="stylesheet" href="<c:url value="/resources/assets/css/theme.bundle.css" />" id="stylesheetLight"/>
    <link rel="stylesheet" href="<c:url value="/resources/assets/css/theme-dark.bundle.css" />" id="stylesheetDark"/>

    <style>body {
        display: none;
    }</style>

    <!-- Title -->
    <title>Content</title>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-156446909-1"></script>
    <script>window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag("js", new Date());
    gtag("config", "UA-156446909-1");</script>
</head>
<body>

<!-- MODALS -->
<!-- Modal: Members -->

<!-- NAVIGATION -->

<nav class="navbar navbar-vertical fixed-start navbar-expand-md " id="sidebar">
    <div class="container-fluid">

        <!-- Toggler -->
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#sidebarCollapse"
                aria-controls="sidebarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Brand -->
        <a class="navbar-brand" href="index.html">
            <img src="<c:url value="/resources/assets/img/logo.svg" />" class="navbar-brand-img mx-auto" alt="...">
        </a>

        <!-- User (xs) -->

        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidebarCollapse">

            <!-- Form -->
            <form class="mt-4 mb-3 d-md-none">
                <div class="input-group input-group-rounded input-group-merge input-group-reverse">
                    <input class="form-control" type="search" placeholder="Search" aria-label="Search">
                    <div class="input-group-text">
                        <span class="fe fe-search"></span>
                    </div>
                </div>
            </form>

            <!-- Navigation -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="#sidebarDashboards" data-bs-toggle="collapse" role="button"
                       aria-expanded="true" aria-controls="sidebarDashboards">
                        <i class="fe fe-lock"></i> Film
                    </a>
                    <div class="collapse show" id="sidebarDashboards">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a href="/" class="nav-link active">
                                    Home
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div> <!-- / .navbar-collapse -->

    </div>
</nav>
<nav class="navbar navbar-vertical navbar-vertical-sm fixed-start navbar-expand-md " id="sidebarSmall">
    <div class="container-fluid">

        <!-- Toggler -->
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#sidebarSmallCollapse"
                aria-controls="sidebarSmallCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Brand -->
        <a class="navbar-brand" href="index.html">
            <img src="<c:url value="/resources/assets/img/logo.svg" />" class="navbar-brand-img
          mx-auto" alt="...">
        </a>


        <div class="collapse navbar-collapse" id="sidebarSmallCollapse">

            <!-- Push content down -->
            <div class="mt-auto"></div>

            <!-- Customize -->
            <div class="mb-4" data-bs-toggle="tooltip" data-bs-placement="right"
                 data-template='<div class="tooltip d-none d-md-block" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'
                 title="Customize">
                <a class="btn w-100 btn-primary" data-bs-toggle="offcanvas" href="#offcanvasDemo"
                   aria-controls="offcanvasDemo">
                    <i class="fe fe-sliders"></i> <span class="d-md-none ms-2">Customize</span>
                </a>
            </div>

            <!-- User (md) -->
            <div class="navbar-user d-none d-md-flex flex-column" id="sidebarSmallUser">

                <!-- Icon -->
                <a class="navbar-user-link mb-3" data-bs-toggle="offcanvas" href="#sidebarOffcanvasSearch"
                   aria-controls="sidebarOffcanvasSearch">
                <span class="icon">
                  <i class="fe fe-search"></i>
                </span>
                </a>

                <!-- Icon -->
                <a class="navbar-user-link mb-3" data-bs-toggle="offcanvas" href="#sidebarOffcanvasActivity"
                   ria-controls="sidebarOffcanvasActivity">
                <span class="icon">
                  <i class="fe fe-bell"></i>
                </span>
                </a>

                <!-- Dropup -->
                <div class="dropend">

                    <!-- Toggle -->
                    <a href="#" id="sidebarSmallIconCopy" class="dropdown-toggle" role="button"
                       data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <div class="avatar avatar-sm avatar-online">
                            <img src="<c:url value="/resources/assets/img/avatars/profiles/avatar-1.jpg" />"
                                 class="avatar-img rounded-circle" alt="...">
                        </div>
                    </a>

                    <!-- Menu -->
                    <div class="dropdown-menu" aria-labelledby="sidebarSmallIconCopy">
                        <a href="profile-posts.html" class="dropdown-item">Profile</a>
                        <a href="account-general.html" class="dropdown-item">Settings</a>
                        <hr class="dropdown-divider">
                        <a href="sign-in.html" class="dropdown-item">Logout</a>
                    </div>

                </div>

            </div>

        </div> <!-- / .navbar-collapse -->

    </div>
</nav>
<nav class="navbar navbar-expand-lg " id="topnav">
    <div class="container">

        <!-- Toggler -->
        <button class="navbar-toggler me-auto" type="button" data-bs-toggle="collapse" data-bs-target="#navbar"
                aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Brand -->
        <a class="navbar-brand me-auto" href="index.html">
            <img src="<c:url value="/resources/assets/img/logo.svg" />" alt="..." class="navbar-brand-img">
        </a>

    </div> <!-- / .container -->
</nav>


<!-- MAIN CONTENT -->
<div class="main-content">

    <nav class="navbar navbar-expand-md navbar-light d-none d-md-flex" id="topbar" style="min-height: 50px;">
        <div class="container-fluid" style="float: right;">
        </div>
    </nav>


    <!-- HEADER -->
    <div class="header">
        <div class="container-fluid">

            <!-- Body -->
            <div class="header-body">
                <div class="row align-items-end">
                    <div class="col">

                        <!-- Pretitle -->
                        <h6 class="header-pretitle">
                            Page
                        </h6>

                        <!-- Title -->
                        <h1 class="header-title">
                            ${title}
                        </h1>

                    </div>
                </div> <!-- / .row -->
            </div> <!-- / .header-body -->

        </div>
    </div>

    <div class="container-fluid">